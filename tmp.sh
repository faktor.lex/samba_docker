mkdir -p /etc/samba-dc.dock/ /var/lib/samba-dc.dock/ /var/cache/samba-dc.dock/

docker run -it --entrypoint bash --net=host --name=test_samba -v /etc/samba-dc.dock/:/etc/samba/ -v /var/lib/samba-dc.dock/:/var/lib/samba -v /var/cache/samba-dc.dock/:/var/cache/samba faktorlex/alt_samba:test-dc

docker run -it --net=host --cap-add SYS_ADMIN --name=test_samba-dc -v /etc/samba-dc.dock/:/etc/samba/ -v /var/lib/samba-dc.dock/:/var/lib/samba -v /var/cache/samba-dc.dock/:/var/cache/samba faktorlex/alt_samba:test-dc

CONTAINER="^test$"

if [ ! "$(docker ps -aq -f name=$CONTAINER)" ]; then
    if [ "$(docker ps -aq -f status=exited -f name=$CONTAINER)" ]; then
        # cleanup
        docker rm $CONTAINER
    fi
    # run your container
    docker run -d --name $CONTAINER my-docker-image
fi
