#!/bin/bash

#Checking the main outgoing interface and network address

INTERFACES_LINE_FIND=$(grep -e '^\s*interfaces\s*=' /etc/samba/smb.conf -n)
INTERFACES_LINE_ADD="$(ip  r get 1.0.0.0 | grep 1.0.0.0 | sed 's/.*dev //g; s/ .*//g') $(ip r get 1.0.0.0 | grep 1.0.0.0 | sed 's/.*src //g; s/ .*//g;')"

if [ -z "$INTERFACES_LINE_FIND" ]; then
    cp /etc/samba/smb.conf /etc/samba/smb.conf.bk_docker
    sed -i -e "/\s*\[global\]/ainterfaces = $INTERFACES_LINE_ADD" /etc/samba/smb.conf 
else
    echo -e " [Samba Docker Info]
 Explicit parameter 'interfaces' found in smb.conf
\t$INTERFACES_LINE_FIND
 Recommendation:
\tinterfaces = $INTERFACES_LINE_ADD
 Make sure your DNS settings are correct.
"
fi

#Checking for a configured samba and starting the service if there is a configuration file

#If the samba server has never been started, check for the existence of the directory and create
[ -d /var/cache/samba ] || mkdir -p /var/cache/samba

#choose criterion running and configured samba
while [ ! -f  /var/lib/samba/private/krb5.conf ]; do echo -n "Waiting for provision: " &&  date +%H:%M:%S && sleep 1; done

testparm -s && /usr/sbin/samba -i